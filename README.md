# MCS

A small script in Norwegian for administering a minecraft instance on a linux server, meant for private use.
(I know about  http://msmhq.com/, but thought it too big and complex to be able to hack it myself.)

This can start one instance at a time, has only one server jar version at a time, but lets you choose between
multiple saved worlds, and take backups.

Contains the admin script and some skeleton files to start a new world.

The basic options are in place and working, but this is definitely a work in progress.

# Setup

I recommend to create a dedicated user on your machine called "minecraft" or similar.
Also - use `screen` when using this script, so you can reconnect later.

Clone the repo into the users home dir. It will create a directory `~/minecraft/`.

I would also recommend to add the `bin` directory to your PATH, or make an alias to run mcs script, eg.

`alias mcs='~/minecraft/bin/mcs.sh'`

## Dependencies
* bash
* wget
* (screen)

# Usage

Below I will assume you use an alias `mcs`to start the script.

## Help

`mcs -h` - will list the options with a short description

`mcs jar download` will ask you for version number, and download the specified version

`mcs start myworld` starts a new world, presenting the configuration file in your editor

`mcs status` shows if a world is running or not

`mcs stop myworld` stops the server

Use the help option `-h` for more, hopefully self describing, commands and options


# TODOs

* translate to English
* use screen for running the server
* add bash completion
* add install-function (for PATH or alias, bash completion, possibly cron-backup)

