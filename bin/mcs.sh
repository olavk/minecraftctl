#!/bin/bash

#===========================================================
# Deklarasjon av variable + evt. debug
#===========================================================

[[ -n "$DEBUG" ]] && set -x # turn -x on if DEBUG is set to a non-empty string
[[ -n "$NOEXEC" ]] && set -n # turn -n on if NOEXEC is set to a non-empty string
#set -o nounset # Feiler hvis man prøver å bruke en uinitialisert variabel
#set -o errexit # Avslutter umiddelbart hvis et statement returnerer false

HOMEDIR=~/minecraftctl

#===========================================================
# Deklarasjon av funksjoner
#===========================================================


Backup() {
    local name=$1
    [[ -z "$name" ]] && Fail "Navn på verden må angis"
    local timestamp=$(date -Iseconds)
    Status && Fail "Verden må være stoppet."

    # Ta backup
    cp -rp ${HOMEDIR}/worlds/$name ${HOMEDIR}/worlds/$name-${timestamp}
    if [[ $? -eq 0 ]] ; then
        echo "Backup tatt OK til katalog ${HOMEDIR}/worlds/$timestamp"
        exit 0
    else
        echo "Noe gikk galt under backup-kjøring! Sjekk manuelt i katalogen ${HOMEDIR}/worlds"
        exit 1
    fi
}

CheckJar() {
    readlink -f ${HOMEDIR}/jars/current.jar  | egrep minecraft_server[0-9.]+jar
    if [[ $? -ne 0 ]] ; then
        echo -e "\n\n*****  WARNING *****\n"
        echo "No minecraft jar found in ${HOMEDIR}/jars."
        echo "Use the jar-option to download."
        echo
    fi
}

Create() {
    local name=$1
    [[ -z "$name" ]] && Fail "Navn på verden må angis"
    mkdir -p ${HOMEDIR}/
    cp -rp ${HOMEDIR}/skel ${HOMEDIR}/worlds/$name || Fail "Kopiering av skel til verden $name feilet!"
    perl -pi -e "s/level-name=.*/level-name=$name/" ${HOMEDIR}/worlds/$name/server.properties || Fail "Kunne ikke endre servernavn!"
    echo "Verden $name opprettet."
    echo "Bruk edit-kommandoen for å redigere server.properties før du starter."
}

Delete() {
    local name=$1
    [[ -z "$name" ]] && Fail "Name of world must be specified"
    printf "are you SURE you want to delete $name? (y/n): "
    read svar
    [[ "$svar" != y ]] && Fail "Aborting."
    rm -r ${HOMEDIR}/worlds/$name || Fail "Delete of  world $name failed!"
    echo "World $name deleted."
}

Edit() {
    local name=$1
    [[ -z "$name" ]] && Fail "Name of world must be specified"
    echo "Press ENTER to open server.properties in an editor. Please keep \"level-name\" unchanged, så it matches the directory name."
    read svar
    ${EDITOR:-vi} ${HOMEDIR}/worlds/$name/server.properties || Fail "Could not open server.properties under $name for editing."
}

Fail() {
    echo "$1"
    exit 1
}

GetPid() {
    pgrep -f '^java.*minecraft.jar'
}

Jar() {
    local cmd=$1
    local version
    if [[ "$cmd" = list ]] ; then
        ls -l ${HOMEDIR}/jars
    elif [[ "$cmd" = download ]] ; then
        read -p "Enter desired version (e.g. 1.11.2)> " version
        wget -O ${HOMEDIR}/jars/minecraft_server.${version}.jar https://s3.amazonaws.com/Minecraft.Download/versions/${version}/minecraft_server.${version}.jar
        if [[ $? -ne 0 ]] ; then
            Fail "Download failed! Exiting"
        else
            echo "Download OK. Making softlink."
            rm ${HOMEDIR}/jars/current.jar ; ln -s ${HOMEDIR}/jars/minecraft_server.${version}.jar ${HOMEDIR}/jars/current.jar
            ls -l ${HOMEDIR}/jars
        fi
    else
        Fail "Illegal command \"$cmd\". Legal values are \"list\" or \"download\"."
    fi
}

List() {
    ls ${HOMEDIR}/worlds || Fail "Could not find worlds directory"
}

Log() {
    local name=$1
    [[ -z "$name" ]] && Fail "Name of world must be provided"
    less +G ${HOMEDIR}/worlds/$name/logs/server.log || Fail "Could not find log file under $name"
}

Status() {
    local pid=$(GetPid)
    if [[ -n "$pid" ]]; then
        echo "Minecraft server is up on pid $pid, world $(basename $(readlink -f /proc/$pid/cwd))."
        return 0
    else
        echo "No running Minecraft server found."
        return 1
    fi
}

Start() {
    local name=$1
    [[ -z "$name" ]] && Fail "Navn på verden må angis"
    cd ${HOMEDIR}/worlds/$name || Fail "Kunne ikke bytte til katalog ${HOMEDIR}/worlds/$name"
    local pid=$(GetPid)
    if [[ -z "$pid" ]] ; then
        nohup java -Xmx1G -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalPacing -XX:+AggressiveOpts -jar ./minecraft.jar nogui >> logs/server.log 2>&1 &
        echo "Minecraft server starter på pid $(GetPid). Bruk \"status\" og \"log\" for å sjekke status."
        exit 0
    else
        echo "En verden kjører allerede på pid $pid. Avsluter uten å starte."
        exit 1
    fi
}

Stop() {
    local pid=$(pgrep -f '^java.*minecraft.jar')
    echo "Killing $pid"
    kill $pid
}

Usage() {
    echo "SYNOPSIS"
    echo "    $(basename $0) <backup|create|delete|edit|jar|list|log|status|start|stop>"
    echo
    echo "PARAMETRS"
    echo "    $(basename $0) backup <name>"
    echo "        Copies a world directory into a new timestamped directory."
    echo "        All worlds must be stopped."
    echo "    $(basename $0) create <name>"
    echo "        Creares a new world. Name myst be specified and cannot already exist."
    echo "    $(basename $0) delete <name>"
    echo "        Deletes world. Cannot be undone!"
    echo "    $(basename $0) edit <name>"
    echo "        Lets you edit server.properties for the specified world."
    echo "        For the change to take effect, you must stop and start the world."
    echo "    $(basename $0) jar <list|download>"
    echo "        Lists installed versjon or lets you enter a version and have it downloaded and installed"
    echo "    $(basename $0) list"
    echo "        Listes all created worlds"
    echo "    $(basename $0) log <name>"
    echo "        Shows log for specified world"
    echo "    $(basename $0) status"
    echo "        Shows status, if a server is running or not"
    echo "    $(basename $0) start <name>"
    echo "        Starts world with specified name"
    echo "    $(basename $0) stop <name>"
    echo "        Stops specified world. Can be started to resume from where you left off."
    echo
    echo "USAGE"
    echo "    The git repositort must be cloned into the top level of the home directory and will be named \"minecraftctl\"."
    echo "    $(basename $0) can create, run, and administer worlds."
    echo "    Supports only one concurrent running world per user account."
}



#===========================================================
# Hovedprogram
#===========================================================

[[ ! -d ${HOMEDIR}/skel ]] && Fail "This git epo must be cloned under your homedir,  named '${HOMEDIR}'. Avslutter."

CMD=$1
shift

CheckJar

case "$CMD" in
    backup)
        Backup $* ;;
    create)
        Create $* ;;
    delete)
        Delete $* ;;
    edit)
        Edit $* ;;
    jar)
        Jar $* ;;
    list)
        List ;;
    log)
        Log $* ;;
    start)
        Start $* ;;
    status)
        Status ;;
    stop)
        Stop ;;
    *)
        Usage ;;
esac

